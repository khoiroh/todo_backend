package com.app.Presensi_ToDoList.service;

import com.app.Presensi_ToDoList.dto.LoginDTO;
import com.app.Presensi_ToDoList.exception.InternalEror;
import com.app.Presensi_ToDoList.exception.NotFoundException;
import com.app.Presensi_ToDoList.jwt.JwtProvider;
import com.app.Presensi_ToDoList.model.Login;
import com.app.Presensi_ToDoList.repository.LoginRepository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Alur pembuatan 4

@Service
public class LoginServiceImpl implements LoginService{
// untuk memanggil implements nya
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserDetailServiceImpl userDetailService;
    @Autowired
    JwtProvider jwtProvider;
    @Autowired
    LoginRepository loginRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    private static final String DONWLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/todo-presensi.appspot.com/o/%s?alt=media";

    @Override

    public Map<String, Object> login(LoginDTO loginDTO) {
        String token = authories(loginDTO.getEmail(), loginDTO.getPassword());
        Login login1 = loginRepository.findByEmail(loginDTO.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", login1);
        return response;
    }

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalEror("Email Or Password Not Found");
        }
        UserDetails userDetails = userDetailService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);

    }

    @Override
    public Login addLogin(Login login) {
        String email = login.getEmail();
        login.setPassword(passwordEncoder.encode(login.getPassword()));
        var validasi = loginRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalEror("Maaf Email sudah digunakan");
        }
        return loginRepository.save(login);
    }

    @Override
    public Login getLoginById(Long id) {
        return loginRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ditemukan"));

    }

    private String convertToBase64Url(MultipartFile file) {
        String url = "";
        try{
            byte[] byteData = Base64.encodeBase64(file.getBytes());
            String result = new String(byteData);
            url = "data:" + file.getContentType() + ";base64," + result;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            return  url;
        }

    }

    private String imageConverter(MultipartFile multipartFile){
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = converToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalEror("Error upload file");
        }
    }

    private File converToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }
    private String uploadFile(File file, String fileName) throws IOException{
        BlobId blobId = BlobId.of("todo-presensi.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/ServiceAccount.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DONWLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }
    private String getExtenions(String fileName){
        return fileName.split("\\.")[0];
    }

    @Override
    public List<Login> getAllLogin() {
        return loginRepository.findAll();
    }

    @Transactional
    @Override
    public Login editLogin(Long id, Login login, MultipartFile multipartFile) {
        Login update = loginRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
        String url = convertToBase64Url(multipartFile);
        login.setPassword(passwordEncoder.encode(login.getPassword()));
        update.setPassword(login.getPassword());
        update.setNama(login.getNama());
        update.setAlamat(login.getAlamat());
        update.setTelepon(login.getTelepon());
        update.setProfile(url);
        return loginRepository.save(update);
    }

    @Override
    public void deleteLoginById(Long id) {
        loginRepository.deleteById(id);
    }
}
