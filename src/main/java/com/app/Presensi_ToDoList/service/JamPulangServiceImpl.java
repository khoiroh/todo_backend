package com.app.Presensi_ToDoList.service;

import com.app.Presensi_ToDoList.dto.JmPulangDTO;
import com.app.Presensi_ToDoList.enumeted.Login.JamType;
import com.app.Presensi_ToDoList.exception.InternalEror;
import com.app.Presensi_ToDoList.exception.NotFoundException;
import com.app.Presensi_ToDoList.model.JamPulang;
import com.app.Presensi_ToDoList.model.Presensi;
import com.app.Presensi_ToDoList.repository.JamPulangRepository;
import com.app.Presensi_ToDoList.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

//Alur pembuatan 35

@Service
public class JamPulangServiceImpl implements JamPulangService{

    private static final int HOUR = 3600 * 1000;

    @Autowired
    JamPulangRepository jamPulangRepository;

    @Autowired
    LoginRepository loginRepository;

    @Override
    public JamPulang addJamPulang(JmPulangDTO jmPulangDTO) {
        JamPulang jamPulang = new JamPulang();
        jamPulang.setPulang(new Date(new Date().getTime() + 7 * HOUR));
        jamPulang.setUserId(loginRepository.findById(jmPulangDTO.getUserId()).orElseThrow(() -> new NotFoundException("Error")));
        jamPulang.setRole(JamType.PULANG);
        return jamPulangRepository.save(jamPulang);
    }

    @Override
    public Page<JamPulang> findByNama(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return jamPulangRepository.findByNama(userId, pageable);
    }

    @Override
    public void deleteJamPulangById(Long id) {
        jamPulangRepository.deleteById(id);
    }
}
