package com.app.Presensi_ToDoList.service;

import com.app.Presensi_ToDoList.model.Login;
import com.app.Presensi_ToDoList.model.UserPrinciple;
import com.app.Presensi_ToDoList.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

//Alur pembuatan 24

@Service
public class UserDetailServiceImpl implements UserDetailsService {

  @Autowired
  private LoginRepository loginRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Login login = loginRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("username not found"));
    return UserPrinciple.bulid(login);
  }
}
