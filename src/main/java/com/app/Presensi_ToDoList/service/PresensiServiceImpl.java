package com.app.Presensi_ToDoList.service;

import com.app.Presensi_ToDoList.dto.JmPulangDTO;
import com.app.Presensi_ToDoList.dto.PresensiDTO;
import com.app.Presensi_ToDoList.enumeted.Login.JamType;
import com.app.Presensi_ToDoList.exception.InternalEror;
import com.app.Presensi_ToDoList.exception.NotFoundException;
import com.app.Presensi_ToDoList.model.Presensi;
import com.app.Presensi_ToDoList.model.TodoList;
import com.app.Presensi_ToDoList.repository.LoginRepository;
import com.app.Presensi_ToDoList.repository.PresensiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

//Alur pembuatan 28

@Service
public class PresensiServiceImpl implements PresensiService{

    private static final int HOUR = 3600 * 1000;
    @Autowired
    PresensiRepository presensiRepository;

    @Autowired
    LoginRepository loginRepository;

    @Override
    public Presensi absenMasuk(PresensiDTO presensiDTO) {
        Presensi presensi = new Presensi();
        presensi.setStatus(presensiDTO.getStatus());
        presensi.setMasuk(new Date(new Date().getTime() + 7 * HOUR));
        presensi.setUserId(loginRepository.findById(presensiDTO.getUserId()).orElseThrow(() -> new NotFoundException("Error")));
        presensi.setRole(JamType.MASUK);
        return presensiRepository.save(presensi);
    }

    @Override
    public Presensi absenPulang(PresensiDTO presensiDTO) {
        Presensi presensi = new Presensi();
        presensi.setStatus(presensiDTO.getStatus());
        presensi.setMasuk(new Date(new Date().getTime() + 7 * HOUR));
        presensi.setUserId(loginRepository.findById(presensiDTO.getUserId()).orElseThrow(() -> new NotFoundException("Error")));
        presensi.setRole(JamType.PULANG);
        return presensiRepository.save(presensi);
    }

    @Override
    public Page<Presensi> findByNama(Long page, Long userId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return presensiRepository.findByNama(userId, pageable);
    }

    @Override
    public void deletePresensiById(Long id) {presensiRepository.deleteById(id);
    }

}
