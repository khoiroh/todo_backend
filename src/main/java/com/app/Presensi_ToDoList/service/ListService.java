package com.app.Presensi_ToDoList.service;

import com.app.Presensi_ToDoList.dto.ListDTO;
import com.app.Presensi_ToDoList.model.Login;
import com.app.Presensi_ToDoList.model.Presensi;
import com.app.Presensi_ToDoList.model.TodoList;
import org.springframework.data.domain.Page;

import java.util.List;

//Alur pembuatan 39

public interface ListService {

    TodoList getListById(Long id);

    TodoList addList (ListDTO listDTO);

    TodoList selesai(Long id);

    List<TodoList> findByNama(Long userId); // relasi

    TodoList editTodoList(Long id, ListDTO listDTO);

    void deleteTodoListById(Long id);

}
