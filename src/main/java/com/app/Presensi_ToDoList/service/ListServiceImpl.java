package com.app.Presensi_ToDoList.service;

import com.app.Presensi_ToDoList.dto.ListDTO;
import com.app.Presensi_ToDoList.exception.InternalEror;
import com.app.Presensi_ToDoList.exception.NotFoundException;
import com.app.Presensi_ToDoList.model.Login;
import com.app.Presensi_ToDoList.model.TodoList;
import com.app.Presensi_ToDoList.repository.ListRepository;
import com.app.Presensi_ToDoList.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

//Alur pembuatan 40

@Service
public class ListServiceImpl implements ListService{

    private static final int HOUR = 3600 * 1000;
    @Autowired
    ListRepository listRepository;

    @Autowired
    LoginRepository loginRepository;

    @Override
    public TodoList getListById(Long id) {
        var respon = listRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        try {
            return listRepository.save(respon);
        } catch (Exception e) {
            throw new InternalEror("Kesalahan Memunculkan Data");
        }
    }

    @Override
    public TodoList addList(ListDTO listDTO) {
        TodoList toDoList1 = new TodoList();
        toDoList1.setTaks(listDTO.getTaks());
        toDoList1.setTanggalDibuat(new Date(new Date().getTime() + 7 * HOUR));
        toDoList1.setUserId(loginRepository.findById(listDTO.getUserId()).orElseThrow(() -> new NotFoundException("Error")));
        return listRepository.save(toDoList1);
    }

    private static final String selesai = "SELESAI";

    @Override
    public TodoList selesai(Long id) {
        TodoList todoList = listRepository.findById(id).orElseThrow(() -> new NotFoundException("eror selesai"));
        todoList.setSelesai(selesai);
        return listRepository.save(todoList);
    }

    @Override
    public List<TodoList> findByNama(Long userId) {
        return listRepository.findByNama(userId);
    }


    @Override
    public TodoList editTodoList(Long id, ListDTO listDTO) {
        TodoList edit = listRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        edit.setTaks(listDTO.getTaks());
        return listRepository.save(edit);
    }

    @Override
    public void deleteTodoListById(Long id) {
        listRepository.deleteById(id);
    }
}
