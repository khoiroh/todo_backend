package com.app.Presensi_ToDoList.service;

import com.app.Presensi_ToDoList.dto.ListDTO;
import com.app.Presensi_ToDoList.dto.PresensiDTO;
import com.app.Presensi_ToDoList.model.Presensi;
import org.springframework.data.domain.Page;

import java.util.List;

//Alur pembuatan 27

public interface PresensiService {
    Presensi absenMasuk (PresensiDTO presensiDTO);
    Presensi absenPulang (PresensiDTO presensiDTO);

    Page<Presensi> findByNama(Long page, Long userId); // relasi

    void deletePresensiById(Long id);

}
