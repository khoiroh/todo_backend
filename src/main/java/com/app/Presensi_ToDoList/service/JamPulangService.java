package com.app.Presensi_ToDoList.service;

import com.app.Presensi_ToDoList.dto.JmPulangDTO;
import com.app.Presensi_ToDoList.dto.PresensiDTO;
import com.app.Presensi_ToDoList.model.JamPulang;
import com.app.Presensi_ToDoList.model.Presensi;
import org.springframework.data.domain.Page;

//Alur pembuatan 34

public interface JamPulangService {
    JamPulang addJamPulang (JmPulangDTO jmPulangDTO);

    Page<JamPulang> findByNama(Long page, Long userId);


    void deleteJamPulangById(Long id);
}
