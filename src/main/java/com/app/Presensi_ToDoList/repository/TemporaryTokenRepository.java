package com.app.Presensi_ToDoList.repository;

import com.app.Presensi_ToDoList.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//Alur pembuatan 22
@Repository
public interface TemporaryTokenRepository extends JpaRepository<TemporaryToken, Long> {


    Optional<TemporaryToken> findByToken(String token);

    Optional<TemporaryToken> findByUserId(Long userId);

}
