package com.app.Presensi_ToDoList.repository;

import com.app.Presensi_ToDoList.model.JamPulang;
import org.hibernate.metamodel.model.convert.spi.JpaAttributeConverter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//Alur pembuatan 33

@Repository
public interface JamPulangRepository extends JpaRepository<JamPulang, Long> {

    @Query(value = "select * from jam_pulang where user_id = ?1", nativeQuery = true)
    Page<JamPulang> findByNama(Long userId, Pageable pageable);
}
