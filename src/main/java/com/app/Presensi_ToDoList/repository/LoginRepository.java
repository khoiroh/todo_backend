package com.app.Presensi_ToDoList.repository;

import com.app.Presensi_ToDoList.model.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

// Alur pembuatan 2
@Repository
public interface LoginRepository extends JpaRepository<Login, Long> {

    Optional<Login> findByEmail(String email);
    Boolean existsByEmail(String email);
}
