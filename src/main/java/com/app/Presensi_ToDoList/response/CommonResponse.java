package com.app.Presensi_ToDoList.response;

//Alur pembuatan 17

public class CommonResponse<T> {
//    untuk response
    private String massage;

    private String status;

    private T data;

    public CommonResponse() {
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
