package com.app.Presensi_ToDoList.controller;

import com.app.Presensi_ToDoList.dto.ListDTO;
import com.app.Presensi_ToDoList.dto.PresensiDTO;
import com.app.Presensi_ToDoList.model.Presensi;
import com.app.Presensi_ToDoList.model.TodoList;
import com.app.Presensi_ToDoList.repository.PresensiRepository;
import com.app.Presensi_ToDoList.response.CommonResponse;
import com.app.Presensi_ToDoList.response.ResponseHelper;
import com.app.Presensi_ToDoList.service.PresensiService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Alur pembuatan 29

@RestController
@RequestMapping("/presensi")
@CrossOrigin(origins = "http://localhost:3000")
public class PresensiController {

//    Menjalankan menthod presensi
    @Autowired
    PresensiService presensiService;

    @Autowired
    ModelMapper modelMapper;

//    method post/masuk untuk menambahkan  jam masuk
    @PostMapping("/masuk")
    public CommonResponse<Presensi> absenMasuk(@RequestBody PresensiDTO presensiDTO) {
        return ResponseHelper.ok(presensiService.absenMasuk(presensiDTO));
    }

//    method post/pulang untuk menambahkan jam pulang
    @PostMapping("/pulang")
    public CommonResponse<Presensi> absenPulang(@RequestBody PresensiDTO presensiDTO) {
        return ResponseHelper.ok(presensiService.absenPulang(presensiDTO));
    }

//    method Get-all untuk melihat semua variable
    @GetMapping("/all-presensi")
    public CommonResponse<Page<Presensi>> findByNama(@RequestParam Long userId, Long page) {
        return ResponseHelper.ok(presensiService.findByNama(page, userId));
    }

//    method delete untuk menghapus variable per id
    @DeleteMapping("/{id}")
    public void deletePresensiById(@PathVariable("id") Long id) {
        presensiService.deletePresensiById(id);
    }

}
