package com.app.Presensi_ToDoList.controller;

import com.app.Presensi_ToDoList.dto.JmPulangDTO;
import com.app.Presensi_ToDoList.dto.PresensiDTO;
import com.app.Presensi_ToDoList.model.JamPulang;
import com.app.Presensi_ToDoList.model.Presensi;
import com.app.Presensi_ToDoList.response.CommonResponse;
import com.app.Presensi_ToDoList.response.ResponseHelper;
import com.app.Presensi_ToDoList.service.JamPulangService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pulangg")
@CrossOrigin(origins = "http://localhost:3000")
public class JamPulangController {

//Alur pembuatan 36

    @Autowired
    JamPulangService jamPulangService;

    @PostMapping
    public CommonResponse<JamPulang> addJamPulang(JmPulangDTO jmPulangDTO) {
        return ResponseHelper.ok(jamPulangService.addJamPulang(jmPulangDTO));
    }

    @GetMapping("/all")
    public CommonResponse<Page<JamPulang>> findByNama(@RequestParam Long userId, Long page) {
        return ResponseHelper.ok(jamPulangService.findByNama(page, userId));
    }

    @DeleteMapping("/{id}")
    public void deleteJamPulangById(@PathVariable("id") Long id) {
        jamPulangService.deleteJamPulangById(id);
    }

}
