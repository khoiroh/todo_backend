package com.app.Presensi_ToDoList.controller;

import com.app.Presensi_ToDoList.dto.ListDTO;
import com.app.Presensi_ToDoList.model.TodoList;
import com.app.Presensi_ToDoList.repository.ListRepository;
import com.app.Presensi_ToDoList.response.CommonResponse;
import com.app.Presensi_ToDoList.response.ResponseHelper;
import com.app.Presensi_ToDoList.service.ListService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

//Alur pembuatan 41

@RestController
@RequestMapping("/list")
@CrossOrigin(origins = "http://localhost:3000")
public class ListController {

//    Menjalankan method todo list

    @Autowired
    ListRepository listRepository;
    @Autowired
    ListService listService;
    @Autowired
    ModelMapper modelMapper;

//    method post untuk menambahkan variable
    @PostMapping
    public CommonResponse<TodoList> addList(@RequestBody ListDTO listDTO) {
        return ResponseHelper.ok(listService.addList(listDTO));
    }

//    method put untuk mengedit variable per id
    @PutMapping("/{id}")
    public CommonResponse<TodoList> editTodoList(@PathVariable("id") Long id, @RequestBody ListDTO listDTO) {
        return ResponseHelper.ok(listService.editTodoList(id, listDTO));
    }

//    method put/selesai untuk mengedit bila sudah selesai per id
    @PutMapping("/selesai/{id}")
    public CommonResponse<TodoList> selesai(@PathVariable("id") Long id) {
        return ResponseHelper.ok(listService.selesai(id));
    }

//    method Get untuk melihat variable per id
    @GetMapping("/{id}")
    public CommonResponse<TodoList> getListById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(listService.getListById(id));
    }

//    method Get untuk melihat semua variable
    @GetMapping("/all")
    public CommonResponse<List<TodoList>> getAllTodoList(Long userId) {
        return ResponseHelper.ok(listService.findByNama(userId));
    }

//    method delete untuk menghapus variable per id
    @DeleteMapping("/{id}")
    public void deleteTodoListById(@PathVariable("id") Long id) {
        listService.deleteTodoListById(id);
    }

}
