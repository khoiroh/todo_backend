package com.app.Presensi_ToDoList.controller;

import com.app.Presensi_ToDoList.dto.EditProfileDto;
import com.app.Presensi_ToDoList.dto.LoginDTO;
import com.app.Presensi_ToDoList.dto.RegistrasiDTO;
import com.app.Presensi_ToDoList.model.Login;
import com.app.Presensi_ToDoList.repository.LoginRepository;
import com.app.Presensi_ToDoList.response.CommonResponse;
import com.app.Presensi_ToDoList.response.ResponseHelper;
import com.app.Presensi_ToDoList.service.LoginService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

//Alur pembuatan 5

@RestController
@RequestMapping("/login1")
@CrossOrigin(origins = "http://localhost:3000")
public class LoginController {

//    Menjalankan Method login
    @Autowired
    LoginRepository loginRepository;

    @Autowired
    LoginService loginService;

    @Autowired
    private ModelMapper modelMapper;

//    method post/sign-in untuk menambahkan login
    @PostMapping("/sign-in")
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDTO loginDTO) {
        return ResponseHelper.ok(loginService.login(loginDTO));
    }

//    method post/sign-up untuk menambahkan register
    @PostMapping("/sign-up")
    public CommonResponse<Login> addLogin(@RequestBody RegistrasiDTO login) {
        return ResponseHelper.ok(loginService.addLogin(modelMapper.map(login, Login.class)));
    }

//    method Get untuk melihat variable per id
    @GetMapping("/{id}")
    public CommonResponse<Login> getLoginById(@PathVariable("id") Long id) {
        return ResponseHelper.ok(loginService.getLoginById(id));
    }

//    method put untuk mengedit/ merubah variable per id
    @PutMapping(consumes = "multipart/form-data", path = "/{id}")
    public CommonResponse<Login> editLogin(@PathVariable("id") Long id, EditProfileDto editProfileDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(loginService.editLogin(id, modelMapper.map(editProfileDto, Login.class), multipartFile));
    }

//    method Get/all untuk melihat semua variable
    @GetMapping("/all")
    public CommonResponse<List<Login>> getAllLogin() {
        return ResponseHelper.ok(loginService.getAllLogin());
    }

//    method delete untuk menghapus variable per id
    @DeleteMapping("/{id}")
    public void deleteLoginById(@PathVariable("id") Long id) {
        loginService.deleteLoginById(id);
    }

}
