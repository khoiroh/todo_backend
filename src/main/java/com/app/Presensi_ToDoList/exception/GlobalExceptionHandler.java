package com.app.Presensi_ToDoList.exception;

import com.app.Presensi_ToDoList.response.ResponseHelper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

//Alur pembuatan 10

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> notFound(NotFoundException notFoundException){
        return ResponseHelper.eror(notFoundException.getMessage(), HttpStatus.NOT_FOUND);}

    @ExceptionHandler(InternalEror.class)
    public ResponseEntity<?> internalEror(InternalEror internalEror){
        return ResponseHelper.eror(internalEror.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);}

    @ExceptionHandler(EmailCondition.class)
    public ResponseEntity<?> EmailCondition(EmailCondition emailCondition){
        return ResponseHelper.eror(emailCondition.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);}
}
