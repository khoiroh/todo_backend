package com.app.Presensi_ToDoList.exception;

//Alur pembuatan 12

public class NotFoundException extends RuntimeException{
    public NotFoundException(String message) {
        super(message);
    }
}
