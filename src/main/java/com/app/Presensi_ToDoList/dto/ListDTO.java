package com.app.Presensi_ToDoList.dto;

import java.util.Date;

//Alur pembuatan 43

public class ListDTO {
//    untuk membuat DTO todo list
    private String taks;

    private Long userId;

    public ListDTO() {
    }

    public String getTaks() {
        return taks;
    }

    public void setTaks(String taks) {
        this.taks = taks;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
