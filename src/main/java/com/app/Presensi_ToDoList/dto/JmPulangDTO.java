package com.app.Presensi_ToDoList.dto;

import com.app.Presensi_ToDoList.enumeted.Login.JamType;

//Alur pembuatan 42

public class JmPulangDTO {

    private Long userId;

    private JamType role;

    public JmPulangDTO() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public JamType getRole() {
        return role;
    }

    public void setRole(JamType role) {
        this.role = role;
    }
}
