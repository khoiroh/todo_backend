package com.app.Presensi_ToDoList.dto;

import com.app.Presensi_ToDoList.enumeted.Login.JamType;

import java.util.Date;

//Alur pembuatan 30

public class PresensiDTO {
// untuk membuat DTO presensi

    private String status;

    private Long userId;

    private JamType role;

    public PresensiDTO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public JamType getRole() {
        return role;
    }

    public void setRole(JamType role) {
        this.role = role;
    }
}
