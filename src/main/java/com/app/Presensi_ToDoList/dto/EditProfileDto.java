package com.app.Presensi_ToDoList.dto;

import com.app.Presensi_ToDoList.enumeted.Login.UserType;

//Alur pembuatan 44

public class EditProfileDto {
// untuk membuat DTO edit profile
    private String password;

    private String nama;

    private String alamat;

    private String telepon;

    private UserType role;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public UserType getRole() {
        return role;
    }

    public void setRole(UserType role) {
        this.role = role;
    }
}
