package com.app.Presensi_ToDoList.model;

import com.app.Presensi_ToDoList.enumeted.Login.JamType;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

//Alur pembuatan 25

@Entity
@Table(name = "presensi")
public class Presensi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status")
    private String status;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @CreationTimestamp
    @Column(name = "tanggal", updatable = false)
    private Date tanggal;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "masuk", updatable = false)
    private Date masuk;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Login userId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private JamType role;

    public Presensi() {
    }

    public Date getMasuk() {
        return masuk;
    }

    public void setMasuk(Date masuk) {
        this.masuk = masuk;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Login getUserId() {
        return userId;
    }

    public void setUserId(Login userId) {
        this.userId = userId;
    }

    public JamType getRole() {
        return role;
    }

    public void setRole(JamType role) {
        this.role = role;
    }

}
