package com.app.Presensi_ToDoList.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

//Alur pembuatan 23

public class UserPrinciple implements UserDetails {
//    untuk membuat login

    private String email;

    private String password;
    private Collection<? extends GrantedAuthority> authority;

    public UserPrinciple(String email, String password, Collection<? extends GrantedAuthority> authority) {
        this.email = email;
        this.password = password;
        this.authority = authority;
    }
    public static UserPrinciple bulid(Login login) {
        var role = Collections.singletonList(new SimpleGrantedAuthority(login.getRole().name()));
        return new UserPrinciple(
                login.getEmail(),
                login.getPassword(),
                role
        );
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
