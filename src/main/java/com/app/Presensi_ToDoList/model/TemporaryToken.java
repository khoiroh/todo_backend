package com.app.Presensi_ToDoList.model;

import javax.persistence.*;
import java.util.Date;

//Alur pembuatan 32

@Entity
@Table(name = "temporary_token")
public class TemporaryToken {
//    untuk membuat token

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String token;

    @Column(name = "expired_date")
    private Date expiredDate;

    @Column(name = "user_id")
    private Long userId;

    public TemporaryToken() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
