package com.app.Presensi_ToDoList.model;

import com.app.Presensi_ToDoList.enumeted.Login.UserType;
import com.fasterxml.jackson.annotation.JsonSubTypes;

import javax.persistence.*;

// Alur pembuatan 1
@Entity
@Table(name = "login")
public class Login {
//    untuk membuat column login
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "email")
    private String email;

    @Column (name = "password")
    private String password;

    @Column (name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;

    @Lob

    @Column(name = "profile")
    private String profile;

    @Column(name = "telepon")
    private String telepon;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserType role;
    public Login() {
    }

    public Login(String email, String password, String nama, String alamat, String profile, String telepon) {

    }

    public UserType getRole() {
        return role;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public void setRole(UserType role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
