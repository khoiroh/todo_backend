package com.app.Presensi_ToDoList.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

//Alur pembuatan 37

@Entity
@Table(name = "list")
public class TodoList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "taks")
    private String taks;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @CreationTimestamp
    @Column(name = "tanggal_dibuat", updatable = false)
    private Date tanggalDibuat;

    @Column(name = "selesai")
    private String selesai;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Login userId;

    public TodoList() {
    }

    public TodoList(String taks, Date tanggalDibuat) {
        this.taks = taks;
        this.tanggalDibuat = tanggalDibuat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaks() {
        return taks;
    }

    public void setTaks(String taks) {
        this.taks = taks;
    }

    public Date getTanggalDibuat() {
        return tanggalDibuat;
    }

    public void setTanggalDibuat(Date tanggalDibuat) {
        this.tanggalDibuat = tanggalDibuat;
    }

    public Login getUserId() {
        return userId;
    }

    public void setUserId(Login userId) {
        this.userId = userId;
    }

    public String getSelesai() {
        return selesai;
    }

    public void setSelesai(String selesai) {
        this.selesai = selesai;
    }
}
