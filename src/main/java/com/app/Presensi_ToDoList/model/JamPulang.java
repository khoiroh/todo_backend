package com.app.Presensi_ToDoList.model;

import com.app.Presensi_ToDoList.enumeted.Login.JamType;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

//Alur pembuatan 31

@Entity
@Table(name = "pulang")
public class JamPulang {
//    membuat method jam pulang
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "pulang", updatable = false)
    private Date pulang;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @CreationTimestamp
    @Column(name = "tanggal", updatable = false)
    private Date tanggal;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Login userId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private JamType role;

    public JamPulang() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPulang() {
        return pulang;
    }

    public void setPulang(Date pulang) {
        this.pulang = pulang;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Login getUserId() {
        return userId;
    }

    public void setUserId(Login userId) {
        this.userId = userId;
    }

    public JamType getRole() {
        return role;
    }

    public void setRole(JamType role) {
        this.role = role;
    }
}
