package com.app.Presensi_ToDoList.jwt;

import com.app.Presensi_ToDoList.exception.InternalEror;
import com.app.Presensi_ToDoList.exception.NotFoundException;
import com.app.Presensi_ToDoList.model.Login;
import com.app.Presensi_ToDoList.model.TemporaryToken;
import com.app.Presensi_ToDoList.repository.LoginRepository;
import com.app.Presensi_ToDoList.repository.TemporaryTokenRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//Alur pembuatan 15

@Component
public class JwtProvider {
    @Autowired
    LoginRepository loginRepository;

    @Autowired
    private TemporaryTokenRepository temporaryTokenRepository;
    private  static  String seccetKey = "belajar spring";

    private static Integer expired = 900000;

    public String generateToken(UserDetails userDetails){
        String token = UUID.randomUUID().toString().replace("-", "");
        Login login = loginRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User notFound generate token"));
        var chekingToken = temporaryTokenRepository.findByUserId(login.getId());
        if (chekingToken.isPresent()) temporaryTokenRepository.deleteById(chekingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(login.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject(String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalEror("Token error parse"));
    }

    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExist = temporaryTokenRepository.findByToken(token).orElse(null);
        if (tokenExist == null) {
            System.out.println("Token Kosong");
            return false;
        }
        if (tokenExist.getExpiredDate().before(new Date())) {
            System.out.println("Token Expired");
            return false;
        }
        return true;
    }

}
